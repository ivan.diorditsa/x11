# Copyright 2014-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require qt qmake [ slot=5 ]

export_exlib_phases src_configure src_compile src_install

SUMMARY="Qt Cross-platform application framework: QtWebChannel module"
DESCRIPTION="
It offers Qt applications a seamless way to publish QObjects for
interaction with HTML/JavaScript clients. These clients can either be inside
local Qt WebViews or any other, potentially remote, client which supports
JavaScript, as long as a communication channel such as WebSocket is available.
"

LICENCES+=" GPL-2"
MYOPTIONS="doc examples"

DEPENDENCIES="
    build:
        doc? ( x11-libs/qttools:${SLOT}[>=5.11.0] )
    build+run:
        x11-libs/qtbase:${SLOT}[>=${PV}]
        x11-libs/qtdeclarative:${SLOT}[>=${PV}]
"
qtwebchannel_src_configure() {
    if option examples ; then
        EQMAKE_PARAMS+=( QT_BUILD_PARTS+=examples )
    else
        EQMAKE_PARAMS+=( QT_BUILD_PARTS-=examples )
    fi

    qmake_src_configure
}

qtwebchannel_src_compile() {
    default

    option doc && emake docs
}

qtwebchannel_src_install() {
    default

    if option doc ; then
        dodoc doc/${PN}.qch
        docinto html
        dodoc -r doc/${PN}
    fi

    # remove references to build dir
    edo sed -i -e "/^QMAKE_PRL_BUILD_DIR/d" "${IMAGE}"/usr/$(exhost --target)/lib/libQt5*.prl
}

