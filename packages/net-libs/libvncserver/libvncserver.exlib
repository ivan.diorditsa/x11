# Copyright 2008 Bo Ørsted Andresen <zlin@exherbo.org>
# Copyright 2013 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PNV="LibVNCServer-${PV}"

require github [ user=LibVNC tag=${MY_PNV} ] \
        cmake [ api=2 ] \
        option-renames [ renames=[ 'gnutls providers:gnutls' ] ]

SUMMARY="Library for easy implementation of a RDP/VNC (Remote Desktop Protocol/Virtual Network Computing) server"
DESCRIPTION="
VNC is a set of programs using the RFB (Remote Frame Buffer) protocol. They are
designed to "export" a frame buffer via net. It is already in wide use for
administration, but it is not that easy to program a server yourself.
"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="
    sasl
    systemd
    ( providers: gnutls libressl openssl ) [[ number-selected = exactly-one ]]
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        app-arch/lzo:2
        media-libs/libpng:=
        sys-libs/zlib
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        providers:gnutls? (
            dev-libs/gnutls[>=2.4.0]
            dev-libs/libgcrypt[>=1.4.0]
        )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
        sasl? ( net-libs/cyrus-sasl )
        systemd? ( sys-apps/systemd )
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_SHARED_LIBS:BOOL=TRUE
    -DWITH_24BPP:BOOL=TRUE
    -DWITH_FFMPEG:BOOL=FALSE
    -DWITH_IPv6:BOOL=TRUE
    -DWITH_JPEG:BOOL=TRUE
    -DWITH_LZO:BOOL=TRUE
    -DWITH_PNG:BOOL=TRUE
    -DWITH_SDL:BOOL=FALSE
    -DWITH_THREADS:BOOL=TRUE
    -DWITH_TIGHTVNC_FILETRANSFER:BOOL=TRUE
    -DWITH_WEBSOCKETS:BOOL=TRUE
    -DWITH_ZLIB:BOOL=TRUE
)
CMAKE_SRC_CONFIGURE_OPTION_WITHS=(
    'providers:gnutls GCRYPT'
    'providers:gnutls GNUTLS'
    '!providers:gnutls OPENSSL'
    'sasl SASL'
    'systemd SYSTEMD'
)

