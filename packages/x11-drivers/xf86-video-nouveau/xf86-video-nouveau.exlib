# Copyright 2008-2011 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require xorg

export_exlib_phases pkg_setup

if ever is_scm ; then
    SCM_REPOSITORY="https://anongit.freedesktop.org/git/nouveau/${PN}.git"
    require scm-git
else
    DOWNLOADS="https://xorg.freedesktop.org/archive/individual/driver/${PNV}.tar.bz2"
fi

SUMMARY="Open Source 2D/3D acceleration for NVIDIA cards"
HOMEPAGE="https://nouveau.freedesktop.org"

UPSTREAM_CHANGELOG="https://cgit.freedesktop.org/nouveau/${PN}/log/"
UPSTREAM_DOCUMENTATION="${HOMEPAGE}/wiki/FAQ [[ note = FAQ ]]"

LICENCES="MIT"
SLOT="0"
MYOPTIONS="
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        x11-proto/xorgproto
    build+run:
        x11-dri/libdrm[>=2.4.60][video_drivers:nouveau(-)]
        x11-libs/libpciaccess[>=0.10]
        x11-server/xorg-server[>=1.8]
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
"

xf86-video-nouveau_pkg_setup() {
    # Driver uses xf86LoadSubModule to load another xorg module at runtime which means that it
    # contains undefined symbols which would cause loading it to fail when built with -Wl,-z,now
    LDFLAGS+=" -Wl,-z,lazy"
}

