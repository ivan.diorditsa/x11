# Copyright 2012 Joonas Sarajärvi <muep@iki.fi>
# Distributed under the terms of GNU General Public License v2

require github [ user=g-truc ] cmake [ api=2 ]

SUMMARY="Header-only C++ mathematics library"
HOMEPAGE+=" https://glm.g-truc.net"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="doc"

DEPENDENCIES=""

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/477e803f388de7ba1ddbf2b46abf0f332eb454b3.patch
)

CMAKE_SRC_CONFIGURE_PARAMS=(
    GLM_DYNAMIC_LIBRARY_ENABLE:BOOL=FALSE
    GLM_STATIC_LIBRARY_ENABLE:BOOL=FALSE
)
CMAKE_SRC_CONFIGURE_TESTS=( '-DGLM_TEST_ENABLE:BOOL=TRUE -DGLM_TEST_ENABLE:BOOL=FALSE' )

src_prepare() {
    cmake_src_prepare

    # perf tests fail, last checked: 0.9.9.3
    edo sed \
        -e '/perf/d' \
        -i test/CMakeLists.txt
}

src_install() {
    cmake_src_install

    edo rm "${IMAGE}"/usr/$(exhost --target)/include/${PN}/detail/*.cpp \
           "${IMAGE}"/usr/$(exhost --target)/include/${PN}/CMakeLists.txt

    if option doc ; then
        insinto /usr/share/doc/${PNVR}
        doins "${CMAKE_SOURCE}"/doc/${PN}.pdf
        insinto /usr/share/doc/${PNVR}/html
        doins "${CMAKE_SOURCE}"/doc/api/*
    fi
}

