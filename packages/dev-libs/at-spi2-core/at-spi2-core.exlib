# Copyright 2012 Marc-Antoine Perennou<Marc-Antoine@Perennou.com>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ] meson test-dbus-daemon systemd-service

export_exlib_phases src_configure src_install

SUMMARY="D-Bus accessibility specifications and registration daemon."

HOMEPAGE="https://wiki.linuxfoundation.org/accessibility/atk/at-spi/at-spi_on_d-bus"

LICENCES="GPL-2"
MYOPTIONS="gobject-introspection gtk-doc
    X [[ presumed = true ]]
    ( linguas: an as ast be bg bn_IN ca ca@valencia cs da de el en_CA en_GB eo es et eu fa fi fr fur
               ga gl gu he hi hu id it ja km kn ko lt lv ml mr ms nb nl or pa pl pt pt_BR ro ru sk
               sl sq sr sr@latin sv ta te tg tr ug uk uz@cyrillic vi zh_CN zh_HK zh_TW )
    ( providers: dbus-daemon [[ description = [ Use the reference implementation for the dbus daemon ] ]]
                 dbus-broker [[ description = [ Use the dbus-broker implementation for the dbus daemon ] ]]
    ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.19.8]
        virtual/pkg-config
        gtk-doc? ( dev-doc/gtk-doc[>=1.25] )
    build+run:
        dev-libs/libffi
        dev-libs/glib:2[>=2.36.0]
        sys-apps/dbus[>=1]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=1.32.0] )
        X? (
            x11-libs/libICE
            x11-libs/libSM
            x11-libs/libxcb
            x11-libs/libX11
            x11-libs/libXau
            x11-libs/libXdmcp
            x11-libs/libXtst
        )
"

MESON_SRC_CONFIGURE_PARAMS=( '-Denable-x11=yes' "-Dsystemd_user_dir=${SYSTEMDUSERUNITDIR}" )
MESON_SRC_CONFIGURE_OPTION_SWITCHES=( 'gobject-introspection introspection yes no' 'gtk-doc docs' 'X x11 yes no' )

at-spi2-core_src_configure() {
    local dbus_daemon="dbus-daemon"
    option providers:dbus-broker && dbus_daemon="dbus-broker"
    meson_src_configure -Ddefault_bus=${dbus_daemon}
}

at-spi2-core_src_install() {
    meson_src_install

    # Don't hardcode a host-dependent path in the dbus service file.
    edo sed \
        -e "s/$(exhost --target)/host/" \
        -i "${IMAGE}"/usr/share/dbus-1/services/org.a11y.Bus.service \
        -i "${IMAGE}"/usr/share/dbus-1/accessibility-services/org.a11y.atspi.Registry.service
}

